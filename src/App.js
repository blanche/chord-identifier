import React from 'react';
import 'fontsource-roboto';
import { Container, Typography, Box } from '@material-ui/core';
import './App.css';

import Piano from './Piano/Piano'

function App() {
  return (
    <Container className="App">
      <Box my={5}>
        <Typography variant="h4" gutterBottom>
          Chord Identifier
        </Typography>
        <Piano/>
      </Box>
    </Container>
  );
}

export default App;
